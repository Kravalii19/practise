import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hide',
  templateUrl: './hide.component.html',
  styleUrls: ['./hide.component.css']
})
export class HideComponent {
 
  constructor() { }
  
   myFun() {
      document.getElementById("demo").style.color = "green";
      document.getElementById("demo").style.fontWeight = "bold";
      document.getElementById("demo").style.fontSize = "300%";
    } 
  
}
